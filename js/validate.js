function ktTrung(id,arr){
    var index = arr.findIndex(function(item){
        return id == item.ma;
    })
    if(index==-1){
        document.getElementById("spanMaSV").innerText="";
        return true;
    }else{
        document.getElementById("spanMaSV").innerText="Mã Sinh Viên Đã Tồn Tại";
        return false;
    }
}

function ktIdCapNhat(id,arr){
    var index = arr.findIndex(function(item){
        return id == item.ma;
    })
    if(index==-1){
        document.getElementById("spanMaSV").innerText="Mã Sinh Viên Không Đúng";
        return false;
    }else{
        document.getElementById("spanMaSV").innerText="";
        return true;
    }
}

function ktDoDai(value,idErr,min,max){
    var length = value.length;
    if(length<min||max<length){
        document.getElementById(idErr).innerText=`Độ Dài Phải Từ ${min} Đến ${max}`;
        return false
    }else{
        document.getElementById(idErr).innerText="";
        return true;
    }
}

function ktSo(value,idErr){
    var reg = /^\d+$/;
    var isNumber = reg.test(value);
    if(isNumber){
        document.getElementById(idErr).innerText="";
        return true
    }else{
        document.getElementById(idErr).innerText=`Trường Hợp Này Phải Là Số`;
        return false;
    }
}

function ktEmail(value){
    const reg =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = reg.test(value);
  if(isEmail){
    document.getElementById("spanEmail").innerText="";
     return true
  }else{
    document.getElementById("spanEmail").innerText=`Email Không Hợp Lệ`;
        return false;
  }
}
function ktPass(value,min){
    var length = value.length;
    if(length<min){
        document.getElementById("spanPass").innerText=`Mật Khẩu Phải có tối thiểu 8 ký tự `;
        return false
    }else{
        document.getElementById("spanPass").innerText="";
        return true;
    }
}

function ktDiem( value,idErr){
    if( 0<=value && value<=10){
        document.getElementById(idErr).innerText="";
        return true;
    }else{
        document.getElementById(idErr).innerText=`Điểm không hợp lệ vui lòng nhập lại điểm.`;
        return false;
    }
}