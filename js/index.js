var dssv = [];
var dssvJson = localStorage.getItem("DSSV");
var svArr = JSON.parse(dssvJson);
if (dssvJson != null) {
    dssv = svArr.map(function (item) {
        return new svmodel(item.ma, item.ten, item.email, item.pass, item.toan, item.ly, item.hoa)
    });
    renderddsv(dssv);
}

// thêm sinh viên
function themSV() {
    var sv = layThongTin();
    var isValid = true;
    isValid = ktTrung(sv.ma, dssv) &&
        ktDoDai(sv.ma, "spanMaSV", 4, 8) &&
        ktSo(sv.ma, "spanMaSV") &
        ktEmail(sv.email) &
        ktPass(sv.pass, 8) &
        ktDiem(sv.toan, "spanToan") &
        ktDiem(sv.ly, "spanLy") &
        ktDiem(sv.hoa, "spanHoa");
    if (isValid) {
        dssv.push(sv);
        var dssvJson = JSON.stringify(dssv);
        localStorage.setItem("DSSV", dssvJson);
        renderddsv(dssv);
    }

}

// xóa sinh viên
function xoaSv(id) {
    var index = viTri(id, dssv)
    if (index != -1) {
        dssv.splice(index, 1);
        renderddsv(dssv);
    }
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dssvJson);
    renderddsv(dssv);

}

// cập nhật sinh viên 
function suaSv(id) {
    var index = viTri(id, dssv);
    if (index == -1) {
        return;
    }
    var arr = dssv[index];
    showTTLenForm(arr);
}

function capNhat() {
    var sv = layThongTin();

    var isValid = true;
    isValid = 
        ktIdCapNhat(sv.ma,dssv)&&
        ktDoDai(sv.ma, "spanMaSV", 4, 8) &&
        ktSo(sv.ma, "spanMaSV") &
        ktEmail(sv.email) &
        ktPass(sv.pass, 8) &
        ktDiem(sv.toan, "spanToan") &
        ktDiem(sv.ly, "spanLy") &
        ktDiem(sv.hoa, "spanHoa");
    if (isValid) {

        var index = viTri(sv.ma, dssv);
        if (index != -1) {
            dssv[index] = sv;
            renderddsv(dssv);
        }
        var dssvJson = JSON.stringify(dssv);
        localStorage.setItem("DSSV", dssvJson);
        renderddsv(dssv);
    }
}