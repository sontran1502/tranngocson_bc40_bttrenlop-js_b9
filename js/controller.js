function layThongTin() {
    const _masv = document.getElementById('txt-masv').value;
    const _ten = document.getElementById('txt-tensv').value;
    const _email = document.getElementById('txt-email').value;
    const _pass = document.getElementById('txt-pass').value;
    const _toan = document.getElementById('txt-toan').value;
    const _ly = document.getElementById('txt-ly').value;
    const _hoa = document.getElementById('txt-hoa').value;

    var sv = new svmodel(_masv, _ten, _email, _pass, _toan, _ly, _hoa);
    return sv;
}

function renderddsv(arr) {
    var content = "";
    for (var i = 0; i < arr.length; i++) {
        var item = arr[i];
        var contenTr = `
        <tr>
        <td>${item.ma}</td>
        <td>${item.ten}</td>
        <td>${item.email}</td>
        <td>${item.tinhDTB().toLocaleString({})}</td>
        <td><button onclick="xoaSv('${item.ma}')" class="btn btn-danger">Xóa</button></td>
        <td> <button onclick="suaSv('${item.ma}')" class="btn btn-warning">sửa</button></td>
        </tr>
        `
        content += contenTr;
    }
    document.getElementById("dssv").innerHTML = `${content}`
}

function viTri(id, arr) {
    var index = -1;
    for (var i = 0; i < arr.length; i++) {
        var item = arr[i];
        if (item.ma == id) {
            index = i;
            break;
        }
    }
    return index;
}

function showTTLenForm(arr) {
    document.getElementById('txt-masv').value = arr.ma;
    document.getElementById('txt-tensv').value = arr.ten;
    document.getElementById('txt-email').value= arr.email;
    document.getElementById('txt-pass').value=arr.pass;
    document.getElementById('txt-toan').value=arr.toan;
    document.getElementById('txt-ly').value=arr.ly;
    document.getElementById('txt-hoa').value=arr.hoa;

}